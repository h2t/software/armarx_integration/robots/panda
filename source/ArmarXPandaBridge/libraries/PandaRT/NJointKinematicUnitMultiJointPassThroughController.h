/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointKinematicUnitMultiJointPassThroughController
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <atomic>

// Simox
#include <VirtualRobot/Robot.h>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>


namespace pandax
{
    TYPEDEF_PTRS_HANDLE(NJointKinematicUnitMultiJointPassThroughControllerConfig);
    class NJointKinematicUnitMultiJointPassThroughControllerConfig : virtual public armarx::NJointControllerConfig
    {
    public:
        std::vector<std::string> deviceNames;
        std::string controlMode;
    };


    TYPEDEF_PTRS_HANDLE(NJointKinematicUnitMultiJointPassThroughController);
    /**
     * @brief The NJointKinematicUnitMultiJointPassThroughController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointKinematicUnitMultiJointPassThroughController:
        virtual public armarx::SynchronousNJointController
    {
    public:
        using ConfigPtrT = NJointKinematicUnitMultiJointPassThroughControllerConfigPtr;

        inline NJointKinematicUnitMultiJointPassThroughController(
            armarx::RobotUnitPtr prov,
            const NJointKinematicUnitMultiJointPassThroughControllerConfigPtr& cfg,
            const VirtualRobot::RobotPtr&);

        inline void rtRun(const IceUtil::Time&, const IceUtil::Time&) override
        {
            ARMARX_CHECK_EQUAL(target.size(), control.size());
            for (size_t i = 0; i < target.size(); i++)
            {
                if (i == 0)
                {
                    //                    ARMARX_INFO << deactivateSpam(1) << " addr " << target.at(i) << " target value: " << control.at(i);
                }
                *target.at(i) = control.at(i);
            }
        }
        inline void rtPreActivateController() override
        {

        }

        void reset()
        {
            ARMARX_CHECK_EQUAL(target.size(), control.size());
            ARMARX_CHECK_EQUAL(target.size(), sensor.size());
            for (size_t i = 0; i < target.size(); i++)
            {
                auto& control = this->control.at(i);
                control = (*sensor.at(i)) * sensorToControlOnActivateFactor;
                if (std::abs(control) < resetZeroThreshold)
                {
                    control = 0;
                }
            }
        }

        //ice interface
        inline std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointKinematicUnitMultiJointPassThroughController";
        }

        void set(const std::string& deviceName, float val)
        {
            size_t i = 0;
            for (auto& name : deviceNames)
            {
                if (name == deviceName)
                {
                    control.at(i) = val;
                    return;
                }
                i++;
            }
            throw armarx::LocalException("Could not find device with name ") << deviceName << " Available devices: " << deviceNames;
        }

        void set(const std::vector<float>& val)
        {
            ARMARX_CHECK_EQUAL(control.size(), val.size());
            for (size_t i = 0; i < control.size(); i++)
            {
                control.at(i) = val.at(i);
            }
        }

    protected:
        Ice::StringSeq deviceNames;
        std::vector<std::atomic<float>> control;
        std::vector<float*> target {nullptr};
        std::vector<const float*> sensor;
        float sensorToControlOnActivateFactor {0};
        float resetZeroThreshold {0};
    };
}
