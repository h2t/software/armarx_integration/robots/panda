/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/util/CPPUtility/trace.h>

#include <ArmarXPandaBridge/libraries/PandaRT/PandaJoint/PandaJointDevice.h>
using namespace pandax;


// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


const float
PandaJointDevice::nullFloat = 0;


PandaJointDevice::PandaJointDevice(const std::string& name, armarx::NameValueMap& ctrlpos, armarx::NameValueMap& ctrlvel, armarx::NameValueMap& ctrltor):
    DeviceBase(name), SensorDevice(name), SensorDeviceTemplate(name), ControlDevice(name)
{
    ARMARX_TRACE;
    jointCtrlPos.map = &ctrlpos;
    jointCtrlVel.map = &ctrlvel;
    jointCtrlTor.map = &ctrltor;
    jointCtrlESt.map = &ctrlvel;
    jointCtrlMSt.map = &ctrlvel;

    jointCtrlPos.val = &jointCtrlPos.targ.position;
    jointCtrlVel.val = &jointCtrlVel.targ.velocity;
    jointCtrlTor.val = &jointCtrlTor.targ.torque;
    jointCtrlESt.val = &nullFloat;
    jointCtrlMSt.val = &nullFloat;

    jointCtrlPos.sensVal = &(sensorValue.position);
    jointCtrlVel.sensVal = &(sensorValue.velocity);
    jointCtrlTor.sensVal = &(sensorValue.torque);
    jointCtrlESt.sensVal = &nullFloat;
    jointCtrlMSt.sensVal = &nullFloat;

    addJointController(&jointCtrlPos);
    addJointController(&jointCtrlVel);
    addJointController(&jointCtrlTor);
    addJointController(&jointCtrlESt);
    addJointController(&jointCtrlMSt);
}


void
PandaJointDevice::rtSetActiveJointController(armarx::JointController* jointCtrl)
{
    ARMARX_TRACE;
    if (rtGetActiveJointController())
    {
        ARMARX_TRACE;
        auto curJointCtrl = dynamic_cast<PandaJointControllerBase*>(rtGetActiveJointController());
        curJointCtrl->map->erase(getDeviceName());
        curJointCtrl->mapVal = nullptr;
    }
    ControlDevice::rtSetActiveJointController(jointCtrl);
    auto newJointCtrl = dynamic_cast<PandaJointControllerBase*>(jointCtrl);
    newJointCtrl->mapVal = &(*(newJointCtrl->map))[getDeviceName()];
    *(newJointCtrl->mapVal) = *(newJointCtrl->sensVal);
}


void
PandaJointDevice::PandaJointControllerBase::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    ARMARX_CHECK_EXPRESSION(mapVal) << "JointBaseCtrl(" << getControlMode()
                                    << ") has null map target value";
    ARMARX_CHECK_EXPRESSION(val) << "JointBaseCtrl(" << getControlMode()
                                 << ") has null target value";

    *mapVal = *val;
}
