/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXPandaBridge::ArmarXObjects::PandaUnit
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/util/CPPUtility/Iterator.h>

#include <ArmarXPandaBridge/components/PandaUnit/PandaUnit.h>


// Simox
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>

// Panda
#include <franka/control_types.h>
#include <franka/exception.h>

// PandaX
#include <ArmarXPandaBridge/libraries/PandaRT/PandaJoint/PandaJointDevice.h>
using namespace pandax;


std::string
PandaUnit::getDefaultName() const
{
    return "PandaUnit";
}


void
PandaUnit::onInitRobotUnit()
{
    robot = cloneRobot();
}


void
PandaUnit::onConnectRobotUnit()
{
    if (not rtThread.joinable())
    {
        rtThread = std::thread{[&]{rtTask();}};
        ARMARX_DEBUG << "RT thread launched.";
    }

    if (not pandaThread.joinable())
    {
        pandaThread = std::thread{[&]{
                try
                {
                    pandaCtrlLoop();
                }
                catch (...)
                {
                    armarx::handleExceptions();
                }
            }};
        ARMARX_DEBUG << "Panda thread launched.";
    }
}


armarx::PropertyDefinitionsPtr
PandaUnit::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PandaUnitPropertyDefinitions(
            getConfigIdentifier()));
}


void
PandaUnit::initializeKinematicUnit()
{
    throwIfInControlThread(BOOST_CURRENT_FUNCTION);
    using IfaceT = armarx::KinematicUnitInterface;

    auto guard = getGuard();
    throwIfStateIsNot(armarx::RobotUnitState::InitializingUnits, __FUNCTION__);
    // Check if unit is already added
    if (getUnit(IfaceT::ice_staticId()))
    {
        return;
    }
    auto unit = createPandaKinematicSubUnit(getIceProperties()->clone());
    // Add
    if (unit)
    {
        addUnit(std::move(unit));
    }
}


armarx::ManagedIceObjectPtr
PandaUnit::createPandaKinematicSubUnit(const Ice::PropertiesPtr& properties, const std::string& positionControlMode, const std::string& velocityControlMode, const std::string& torqueControlMode)
{
    throwIfInControlThread(BOOST_CURRENT_FUNCTION);
    /// TODO move init code to Kinematic sub unit
    using UnitT = PandaKinematicSubUnit;

    // Add ctrl et al
    std::map<std::string, UnitT::ActuatorData> devs;
    Ice::StringSeq posDevs, velDevs, torDevs;
    armarx::NJointControllerBasePtr posNJCtrl, velNJCtrl, torNJCtrl;
    for (const armarx::ControlDevicePtr& controlDevice : _module<Devices>().getControlDevices().values())
    {
        ARMARX_CHECK_EXPRESSION(controlDevice);
        const auto& controlDeviceName = controlDevice->getDeviceName();
        if (!controlDevice->hasTag(armarx::ControlDeviceTags::CreateNoDefaultController))
        {
            armarx::JointController* jointCtrl = controlDevice->getJointController(positionControlMode);
            if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorPosition>())
            {
                posDevs.push_back(controlDeviceName);
            }
            jointCtrl = controlDevice->getJointController(velocityControlMode);
            if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorVelocity>())
            {
                velDevs.push_back(controlDeviceName);
            }
            jointCtrl = controlDevice->getJointController(torqueControlMode);
            if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorTorque>())
            {
                torDevs.push_back(controlDeviceName);
            }
        }
    }

    auto addCtrl = [&](armarx::NJointControllerBasePtr & njctrl, std::string controlMode, const Ice::StringSeq & devs)
    {
        NJointKinematicUnitMultiJointPassThroughControllerConfigPtr config = new NJointKinematicUnitMultiJointPassThroughControllerConfig;
        config->controlMode = controlMode;
        config->deviceNames = devs;
        njctrl = _module<ControllerManagement>().createNJointController(
                     "NJointKinematicUnitMultiJointPassThroughController",
                     getName() + "_" + "NJointKU_MPTCtrl_" + controlMode,
                     config,
                     false, true);
    };
    addCtrl(posNJCtrl, positionControlMode, posDevs);
    addCtrl(velNJCtrl, velocityControlMode, velDevs);
    addCtrl(torNJCtrl, torqueControlMode, torDevs);

    for (const armarx::ControlDevicePtr& controlDevice : _module<Devices>().getControlDevices().values())
    {
        ARMARX_CHECK_EXPRESSION(controlDevice);
        const auto& controlDeviceName = controlDevice->getDeviceName();
        if (!controlDevice->hasTag(armarx::ControlDeviceTags::CreateNoDefaultController))
        {
            UnitT::ActuatorData ad;
            ad.name = controlDeviceName;
            ad.sensorDeviceIndex =
                _module<Devices>().getSensorDevices().has(controlDeviceName) ?
                _module<Devices>().getSensorDevices().index(controlDeviceName) :
                std::numeric_limits<std::size_t>::max();
            {
                armarx::JointController* jointCtrl = controlDevice->getJointController(positionControlMode);
                if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorPosition>())
                {
                    ad.ctrlPos = NJointKinematicUnitMultiJointPassThroughControllerPtr::dynamicCast(posNJCtrl);
                }
            }
            {
                armarx::JointController* jointCtrl = controlDevice->getJointController(velocityControlMode);
                if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorVelocity>())
                {
                    ad.ctrlVel = NJointKinematicUnitMultiJointPassThroughControllerPtr::dynamicCast(velNJCtrl);
                }
            }
            {
                armarx::JointController* jointCtrl = controlDevice->getJointController(torqueControlMode);
                if (jointCtrl && jointCtrl->getControlTarget()->isA<armarx::ControlTarget1DoFActuatorTorque>())
                {
                    ARMARX_CHECK_EXPRESSION(torNJCtrl);
                    ad.ctrlTor = NJointKinematicUnitMultiJointPassThroughControllerPtr::dynamicCast(torNJCtrl);
                    ARMARX_CHECK_EXPRESSION(ad.ctrlTor);
                }
                else
                {
                    ARMARX_INFO << "No torque mode found for device " << controlDeviceName;
                }
            }

            if (ad.ctrlPos || ad.ctrlTor || ad.ctrlVel)
            {
                devs[controlDeviceName] = std::move(ad);
            }
        }
    }

    if (devs.empty())
    {
        ARMARX_IMPORTANT << "No joint devices found - skipping adding of KinematicUnit";
        return nullptr;
    }

    ARMARX_CHECK_EXPRESSION(!devs.empty()) << "no 1DoF ControlDevice found with matching SensorDevice";
    // Add it
    const std::string configName = getProperty<std::string>("KinematicUnitName");
    const std::string confPre = getConfigDomain() + "." + configName + ".";
    // Properties->setProperty(confPre + "MinimumLoggingLevel", getProperty<std::string>("MinimumLoggingLevel").getValue());
    // Fill properties
    properties->setProperty(confPre + "RobotNodeSetName", _module<RobotData>().getRobotNodetSeName());
    properties->setProperty(confPre + "RobotFileName", _module<RobotData>().getRobotFileName());
    properties->setProperty(confPre + "RobotFileNameProject", _module<RobotData>().getRobotProjectName());
    properties->setProperty(confPre + "TopicPrefix", getProperty<std::string>("KinematicUnitNameTopicPrefix"));

    ARMARX_DEBUG << "creating unit " << configName << " using these properties: " << properties->getPropertiesForPrefix("");
    IceInternal::Handle<UnitT> unit = Component::create<UnitT>(properties, configName, getConfigDomain());

    // Fill devices (sensor + controller)
    unit->setupData(
        getProperty<std::string>("RobotFileName").getValue(),
        _module<RobotData>().cloneRobot(),
        std::move(devs),
        _module<Devices>().getControlModeGroups().groups,
        _module<Devices>().getControlModeGroups().groupsMerged,
        dynamic_cast<armarx::RobotUnit*>(this)
    );
    return unit;
}


void
PandaUnit::rtTask()
{
    ARMARX_TRACE;
    ARMARX_ON_SCOPE_EXIT { ARMARX_IMPORTANT << "exiting rt thread";};

    try
    {
        {
            ARMARX_TRACE;
            const auto timing = -armarx::TimeUtil::GetTime(true);
            robotName = getRobotName();
            // Device init.
            {
                const auto timing = -armarx::TimeUtil::GetTime(true);
                // Joints.
                {
                    const auto timing = -armarx::TimeUtil::GetTime(true);
                    ARMARX_INFO << "adding devices for joints:";
                    size_t i = 0;
                    for (const VirtualRobot::RobotNodePtr& node : * (robot->getRobotNodeSet(getProperty<std::string>("RobotNodeSetName").getValue())))
                    {
                        if (node->isRotationalJoint() || node ->isTranslationalJoint())
                        {
                            PandaJointDevicePtr jdev = std::make_shared<PandaJointDevice>(node->getName(), anglesCtrl, velocitiesCtrl, torquesCtrl);
                            jointDevs.add(jdev->getDeviceName(), jdev);
                            addSensorDevice(jdev);
                            addControlDevice(jdev);
                            deviceNameToIndexMap[jdev->getDeviceName()] = i;
                            i++;
                        }
                    }
                    ARMARX_INFO << "adding devices for joints done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
                }

                ARMARX_INFO << "transitioning to " << armarx::RobotUnitState::InitializingUnits << std::flush;
                finishDeviceInitialization();
                ARMARX_INFO << "device init done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
                ARMARX_INFO << "transitioned to " << getRobotUnitState() << std::flush;
            }
            // Unit init.
            {
                ARMARX_TRACE;
                const auto timing = -armarx::TimeUtil::GetTime(true);
                // Resize tripple buffers.
                {
                    const auto timing = -armarx::TimeUtil::GetTime(true);
                    std::array<double, 7> v;
                    v.fill(0.0);
                    torquesTB.reinit(v);
                    anglesTB.reinit(v);
                    velocitiesTB.reinit(v);
                    initializeDefaultUnits();
                    ARMARX_INFO << "resize buffers done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
                }
                ARMARX_INFO << "transitioning to " << armarx::RobotUnitState::InitializingControlThread << std::flush;
                finishUnitInitialization();
                ARMARX_INFO << "transitioned to " << getRobotUnitState() << std::flush;
                ARMARX_INFO << "unit init done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
            }
            // Get initial sensor values.
            {
                ARMARX_TRACE;
                const auto timing = -armarx::TimeUtil::GetTime(true);
                oputdatedSensorValuesTimepoint = nowNS();
                ARMARX_INFO << "fetching initial robot state" << std::flush;
                rtUpdateSensors(true);
                ARMARX_INFO << "fetching initial robot done" << std::flush;
                ARMARX_INFO << "get init sensor values done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
            }
            // Enter RT.
            {
                ARMARX_TRACE;
                ARMARX_INFO << "now transitioning to rt" << std::flush;
                {
                    const auto timing = -armarx::TimeUtil::GetTime(true);
                    finishControlThreadInitialization();
                    ARMARX_INFO << "init rt thread done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
                }
                ARMARX_INFO << "Transitioned to " << getRobotUnitState() << std::flush;
            }
            ARMARX_INFO << "Pre loop done (" << (timing + armarx::TimeUtil::GetTime(true)).toMicroSeconds() << " us)" ;
        }
        // Meassure time.
        IceUtil::Time timeSinceLastIteration;
        IceUtil::Time currentIterationStart;
        IceUtil::Time lastIterationStart = armarx::TimeUtil::GetTime();
        // Loop.
        while (!shutdownRtThread)
        {
            ARMARX_TRACE;
            rtGetRTThreadTimingsSensorDevice().rtMarkRtLoopStart();
            // Time.
            currentIterationStart = armarx::TimeUtil::GetTime();
            timeSinceLastIteration = currentIterationStart - lastIterationStart;
            lastIterationStart = currentIterationStart;
            // Call functions.
            rtSwitchControllerSetup();  // Switch controllers.
            rtResetAllTargets();
            ARMARX_TRACE;
            rtRunNJointControllers(sensorValuesTimestamp, timeSinceLastIteration);  // run NJointControllers.
            rtHandleInvalidTargets();  // Deactivate broken NJointControllers.
            ARMARX_TRACE;
            rtRunJointControllers(sensorValuesTimestamp, timeSinceLastIteration); // << run JointControllers
            // Communicate with the Panda.
            {
                ARMARX_TRACE;
                rtGetRTThreadTimingsSensorDevice().rtMarkRtBusSendReceiveStart();
                ARMARX_ON_SCOPE_EXIT {rtGetRTThreadTimingsSensorDevice().rtMarkRtBusSendReceiveEnd();};
                rtSendCommands();
                rtUpdateSensors(false);
            }
            ARMARX_TRACE;
            rtUpdateSensorAndControlBuffer(sensorValuesTimestamp, timeSinceLastIteration); // << swap out ControllerTargets / SensorValues
            ++iterationCount;
            rtGetRTThreadTimingsSensorDevice().rtMarkRtLoopPreSleep();
            // Sleep remainder.
            // Since the timeserver does not end the sleep on shutdown (and this would block
            // shutdown), we do busy waiting and poll the time.
            const IceUtil::Time sleepUntil = currentIterationStart + getControlThreadTargetPeriod();
            IceUtil::Time currentTime = armarx::TimeUtil::GetTime();
            while (currentTime < sleepUntil)
            {
                ARMARX_TRACE;
                if (shutdownRtThread)
                {
                    return;
                }
                if (currentTime < currentIterationStart)
                {
                    // This fixes sleeping for a long time in case the time server is reset.
                    break;
                }
                std::this_thread::sleep_for(std::chrono::microseconds{100}); // Polling is done with 10kHz
                currentTime = armarx::TimeUtil::GetTime();
            }
        }
    }
    catch (Ice::Exception& e)
    {
        ARMARX_ERROR << "exception in rtTask!\nwhat:\n"
                     << e.what()
                     << "\n\tname: " << e.ice_id()
                     << "\n\tfile: " << e.ice_file()
                     << "\n\tline: " << e.ice_line()
                     << "\n\tstack: " << e.ice_stackTrace()
                     << std::flush;
        throw;
    }
    catch (std::exception& e)
    {
        ARMARX_ERROR << "exception in rtTask!\nwhat:\n" << e.what() << std::flush;
        throw;
    }
    catch (...)
    {
        ARMARX_ERROR << "exception in rtTask!" << std::flush;
        throw;
    }
}


void
PandaUnit::readPandaState(const franka::RobotState& state)
{
    anglesTB.getWriteBuffer() = state.q;
    anglesTB.write();
    velocitiesTB.getWriteBuffer() = state.dq;
    velocitiesTB.write();
    torquesTB.getWriteBuffer() = state.tau_J;
    torquesTB.write();
}


void
PandaUnit::rtUpdateSensors(bool wait)
{
    // Wait for all new data.
    if (wait)
    {
        ARMARX_TRACE;
        std::mutex dummy;
        std::unique_lock<std::mutex> dummlock {dummy};
        std::size_t fails = 0;
        while (!allNewData())
        {
            ARMARX_TRACE;
            ARMARX_IMPORTANT << deactivateSpam() << "waiting for up to date sensor values (iteration " << iterationCount << ") ";
            cvGotSensorData.wait_for(dummlock, std::chrono::milliseconds {10});
            ++fails;
            if (!(fails % 10))
            {
                rtPollRobotState();
            }
        }
    }

    ARMARX_TRACE;
    const IceUtil::Time now = armarx::TimeUtil::GetTime();
    const IceUtil::Time timeSinceLastIteration = now - sensorValuesTimestamp;
    const float dt = static_cast<float>(timeSinceLastIteration.toSecondsDouble());
    sensorValuesTimestamp = now;
    torquesTB.read();
    anglesTB.read();
    velocitiesTB.read();
    // Update joint sensors.
    for (std::size_t i = 0; i < jointDevs.size(); ++i)
    {
        ARMARX_TRACE;
        PandaJointDevice& jdev = *jointDevs.at(i);
        const float deltav = velocitiesTB.getReadBuffer().at(i) - jdev.sensorValue.velocity;
        jdev.sensorValue.acceleration = deltav / dt;
        jdev.sensorValue.position = anglesTB.getReadBuffer().at(i);
        jdev.sensorValue.torque = torquesTB.getReadBuffer().at(i);
        jdev.sensorValue.velocity = velocitiesTB.getReadBuffer().at(i);
    }

    rtReadSensorDeviceValues(sensorValuesTimestamp, timeSinceLastIteration);
}


void
PandaUnit::fillTB(TripleBufferWithGuardAndTime<std::vector<float> >& b, const armarx::NameValueMap& nv, const std::string name) const
{
    ARMARX_TRACE;
    if (skipReport())
    {
        ARMARX_DEBUG << deactivateSpam(10, name) << "Skipped all sensor values for " << name;
        return;
    }
    auto g = b.guard();
    std::stringstream ignored;
    bool someWereIgnored = false;
    for (const auto& a : nv)
    {
        ARMARX_TRACE;
        if (jointDevs.has(a.first))
        {
            b.getWriteBuffer().at(jointDevs.index(a.first)) = a.second;
        }
        else
        {
            ignored << a.first << " -> " << a.second << "\n";
            someWereIgnored = true;
        }
    }
    b.write();
    ARMARX_DEBUG << deactivateSpam(10, name) << "Got new  sensor values for " << name;
    if (someWereIgnored)
    {
        ARMARX_TRACE;
        ARMARX_VERBOSE << deactivateSpam(10, name) << "Ignored sensor values for " << name << ":\n" << ignored.str();
    }
}


bool
PandaUnit::allNewData(bool printMissing) const
{
    bool result = true;
    if (torquesTB.getLastWriteT() <= oputdatedSensorValuesTimepoint)
    {
        ARMARX_TRACE;
        if (printMissing)
        {
            ARMARX_VERBOSE << "the sensor values for joint torques are out of date";
        }
        result = false;
    }
    if (anglesTB.getLastWriteT() <= oputdatedSensorValuesTimepoint)
    {
        ARMARX_TRACE;
        if (printMissing)
        {
            ARMARX_VERBOSE << "the sensor values for joint angles are out of date";
        }
        result = false;
    }
    if (velocitiesTB.getLastWriteT() <= oputdatedSensorValuesTimepoint)
    {
        ARMARX_TRACE;
        if (printMissing)
        {
            ARMARX_VERBOSE << "the sensor values for joint velocities are out of date";
        }
        result = false;
    }

    ARMARX_DEBUG << "allNewData = " << (result ? "true" : "false");
    return result;
}


void
PandaUnit::pandaCtrlLoop()
{
    ARMARX_TRACE;
    using namespace franka;
    ARMARX_DEBUG << "Connecting";
    frankaRobot = std::make_unique<franka::Robot>(getProperty<std::string>("PandaIP").getValue(), franka::RealtimeConfig::kIgnore);
    //    {
    //        static constexpr float l = 1000;
    //        static constexpr float u = 10000;
    //        frankaRobot->setCollisionBehavior(
    //        {l, l, l, l, l, l, l}, {u, u, u, u, u, u, u},
    //        {l, l, l, l, l, l}, {u, u, u, u, u, u}
    //        );
    //    }
    ARMARX_DEBUG << "Connection established";
    auto robotState = frankaRobot->readOnce();
    ARMARX_DEBUG << "Read once";
    readPandaState(robotState);
    ARMARX_DEBUG << "Panda state read";
    PandaControlMode activeCtrlMode = PandaControlMode::None;
    bool pandaCallbackFinishRequested = true;

    auto update = [&](const RobotState & state)
    {
        ARMARX_TRACE;
        readPandaState(state);
        pandaControlState.updateReadBuffer();
        PandaControlState controlState = pandaControlState.getReadBuffer();
        for (unsigned int i = 0; i < controlState.positionTargets.size(); ++i)
        {
            {
                ARMARX_TRACE;
                auto& posTarget = controlState.positionTargets.at(i);
                if (std::isnan(posTarget))
                {
                    posTarget = robotState.q_d.at(i);
                }
            }
            {
                ARMARX_TRACE;
                auto& velTarget = controlState.velocityTargets.at(i);
                if (std::isnan(velTarget))
                {
                    velTarget = 0;
                }
            }
            {
                ARMARX_TRACE;
                auto& torTarget = controlState.torqueTargets.at(i);
                if (std::isnan(torTarget))
                {
                    torTarget = 0;
                }
            }
            //i++; // WARNING! Potential bug! => Check!
        }

        if (activeCtrlMode != controlState.requestedCtrlMode)
        {
            ARMARX_TRACE;
            pandaCallbackFinishRequested = true;
        }

        if (shutdownRtThread)
        {
            ARMARX_TRACE;
            pandaCallbackFinishRequested = true;
        }

        return controlState;
    };

    auto readRobotStateCallback = [&](const RobotState & state)
    {
        ARMARX_TRACE;
        update(state);
        if (pandaCallbackFinishRequested)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << deactivateSpam(0.1) << "Returning from  readRobotStateCallback";
            return false;
        }
        else
        {
            return true;
        }
    };

    auto runPositionCtrl = [&](const RobotState & state, Duration)
    {
        ARMARX_TRACE;
        PandaControlState ctrlState = update(state);
        JointPositions targets(ctrlState.positionTargets);
        if (pandaCallbackFinishRequested)
        {
            return MotionFinished(targets);
        }
        else
        {
            return targets;
        }
    };

    auto runVelocityCtrl = [&](const RobotState & state, Duration)
    {
        ARMARX_TRACE;
        PandaControlState ctrlState = update(state);

        JointVelocities targets(ctrlState.velocityTargets);
        //            ARMARX_DEBUG << deactivateSpam(0.1) << "Velocity target - " << VAROUT(targets.dq.at(0));
        if (pandaCallbackFinishRequested)
        {
            ARMARX_DEBUG << deactivateSpam(0.1) << "Returning from  velocity control";
            return MotionFinished(targets);
        }
        else
        {
            return targets;
        }
    };

    auto runTorqueCtrl = [&](const RobotState & state, Duration)
    {
        ARMARX_TRACE;

        PandaControlState ctrlState = update(state);

        Torques targets(ctrlState.torqueTargets);
        if (pandaCallbackFinishRequested)
        {
            return MotionFinished(targets);
        }
        else
        {
            return targets;
        }
    };

    ARMARX_DEBUG << "Starting loop now";

    while (!shutdownRtThread)
    {
        ARMARX_TRACE;
        PandaControlState controlTargetState = pandaControlState.getUpToDateReadBuffer();
        //            ARMARX_DEBUG  << deactivateSpam(1) << "start of ctrl loop iteration " << VAROUT(int(activeCtrlMode)) << VAROUT(int(controlTargetState.requestedCtrlMode));
        // ARMARX_DEBUG << "Called update";
        try
        {
            ARMARX_TRACE;
            //                if (/*activeCtrlMode != PandaControlMode::None &&*/ controlTargetState.requestedCtrlMode != PandaControlMode::None)
            {
                ARMARX_TRACE;
                pandaCallbackFinishRequested = false;
                // ARMARX_DEBUG << "Choosing control mode";
                switch (controlTargetState.requestedCtrlMode)
                {
                    case PandaControlMode::Position:
                    {
                        ARMARX_TRACE;
                        activeCtrlMode = PandaControlMode::Position;
                        frankaRobot->control(runPositionCtrl);
                    }
                    break;
                    case PandaControlMode::Velocity:
                    {
                        ARMARX_TRACE;
                        activeCtrlMode = PandaControlMode::Velocity;
                        frankaRobot->control(runVelocityCtrl);
                    }
                    break;
                    case PandaControlMode::Torque:
                    {
                        ARMARX_TRACE;
                        activeCtrlMode = PandaControlMode::Torque;
                        frankaRobot->control(runTorqueCtrl);
                    }
                    break;
                    case PandaControlMode::Stop:
                    case PandaControlMode::None:
                    {
                        ARMARX_TRACE;
                        activeCtrlMode = controlTargetState.requestedCtrlMode;
                        frankaRobot->read(readRobotStateCallback);
                        //                            ARMARX_INFO << deactivateSpam(3) << "TODO";
                    }
                    break;
                    default:
                    {
                        ARMARX_TRACE;
                        activeCtrlMode = PandaControlMode::None;

                        ARMARX_INFO << deactivateSpam(1) << "Unknown control mode with number " << (int)(controlTargetState.requestedCtrlMode);
                        usleep(500);
                    }
                }
                // ARMARX_DEBUG << "Chose control mode";
            }
        }
        catch (const franka::ControlException& e)
        {
            const auto& state = frankaRobot->readOnce();
            ARMARX_ERROR << deactivateSpam(1)
                         << "Control exception:\n"
                         << "    what: " << e.what() << '\n'
                         << "    current state: " << state << "\n"
                         << "    log: (" << e.log.size() << ")\n" << ARMARX_STREAM_PRINTER
            {
                for (const auto& [idx, msg] : armarx::MakeIndexedContainer(e.log))
                {
                    out << "        " << idx << " state: \n" << msg.state << '\n';
                }
            }
                    << armarx::GetHandledExceptionString();
            frankaRobot->automaticErrorRecovery();
        }
        catch (...)
        {
            ARMARX_ERROR << deactivateSpam(1) << armarx::GetHandledExceptionString();
            frankaRobot->automaticErrorRecovery();
        }

        activeCtrlMode = PandaControlMode::None;
    }
}


void
PandaUnit::rtSendCommands()
{
    ARMARX_TRACE;
    PandaControlState state;
    state.positionTargets.fill(std::nan("")); // use current position for all unspecified position targets later
    state.velocityTargets.fill(0.0);
    state.torqueTargets.fill(0.0);

    try
    {
        ARMARX_TRACE;
        bool stopRequested = false;
        int positionModeActive = 0;
        int velocityModeActive = 0;
        int torqueModeActive = 0;
        size_t i = 0;
        for (armarx::ControlDevicePtr ctrlDev : rtGetControlDevices())
        {
            ARMARX_TRACE;
            auto ctrl = ctrlDev->rtGetActiveJointController();
            auto target = ctrl->getControlTarget();
            auto modeHash = ctrl->getHardwareControlModeHash();
            if (target->isValid())
            {
                ARMARX_TRACE;
                if (target->isA<armarx::DummyControlTargetEmergencyStop>() or target->isA<armarx::DummyControlTargetStopMovement>())
                {
                    stopRequested = true;
                }
                else if (modeHash == positionModeHash)
                {
                    state.positionTargets.at(i) = target->asA<armarx::ControlTarget1DoFActuatorPosition>()->position;
                    positionModeActive = 1;
                }
                else if (modeHash == velocityModeHash)
                {
                    //                        ARMARX_INFO << deactivateSpam(1) << " velocity addr " << &target->asA<ControlTarget1DoFActuatorVelocity>()->velocity << " target value: " << target->asA<ControlTarget1DoFActuatorVelocity>()->velocity;
                    state.velocityTargets.at(i) = target->asA<armarx::ControlTarget1DoFActuatorVelocity>()->velocity;
                    velocityModeActive = 1;
                }
                else if (modeHash == torqueModeHash)
                {
                    //                        ARMARX_INFO << deactivateSpam(1) << " torque addr " << &target->asA<ControlTarget1DoFActuatorTorque>()->torque << " target value: " << target->asA<ControlTarget1DoFActuatorTorque>()->torque;

                    state.torqueTargets.at(i) = target->asA<armarx::ControlTarget1DoFActuatorTorque>()->torque;
                    torqueModeActive = 1;
                }

            }
            i++;
        }
        ARMARX_TRACE;

        if (stopRequested)
        {
            ARMARX_TRACE;
            frankaRobot->stop();
            state.requestedCtrlMode = PandaControlMode::Stop;
        }
        else if (positionModeActive + velocityModeActive + torqueModeActive > 1)
        {
            ARMARX_ERROR << "More than one control mode is active!";
        }
        else
        {
            ARMARX_TRACE;
            if (positionModeActive)
            {
                state.requestedCtrlMode = PandaControlMode::Position;
            }
            else if (velocityModeActive)
            {
                state.requestedCtrlMode = PandaControlMode::Velocity;
            }
            else if (torqueModeActive)
            {
                state.requestedCtrlMode = PandaControlMode::Torque;
            }
        }


        pandaControlState.getWriteBuffer() = state;
        pandaControlState.commitWrite();
        // send commands to panda!
    }
    catch (...)
    {
        armarx::handleExceptions();
    }
}


bool
PandaUnit::skipReport() const
{
    ARMARX_TRACE;
    static const std::set<armarx::RobotUnitState> reportAcceptingStates
    {
        armarx::RobotUnitState::InitializingControlThread,
        armarx::RobotUnitState::Running
    };
    return !reportAcceptingStates.count(getRobotUnitState());
}


void
PandaUnit::rtPollRobotState()
{
    ARMARX_TRACE;
    if (not isPolling.exchange(true))
    {
        allNewData(true);
    }
}


void
PandaUnit::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
{
    RobotUnit::componentPropertiesUpdated(changedProperties);
}
